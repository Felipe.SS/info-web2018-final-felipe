<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('temporada', function (Blueprint $table) {
        $table->increments('id');
        $table->timestamp('volta')->nullable();
        $table->timestamp('ida')->nullable();
        $table->unsignedInteger('user_id');
        $table->foreign('user_id')->references('id')->on('user');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('temporada');

    }
}
